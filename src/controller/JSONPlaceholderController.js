const axios = require("axios");

module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      res.status(200).json({
        message:
          "aqui estão os usuários captqados da api pública JSONPlaceholder",
        users,
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }
  static async getUsersWebsiteIO(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user) =>
        user.website.endsWith(".io")
      );
      const Banana = users.length;
      res.status(200).json({
        message:
          "AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",
        Banana,
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
  }

  static async getUsersWebsiteCOM(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user) =>
        user.website.endsWith(".com")
      );
      const Banana = users.length;
      res.status(200).json({
        message:
          "AQUI ESTÃO OS USERS COM DOMINIO com. Quantos bananas tem esse dominio:",
        Banana,
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
  }

  static async getUsersWebsiteNET(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user) =>
        user.website.endsWith(".net")
      );
      const Banana = users.length;
      res.status(200).json({
        message:
          "AQUI ESTÃO OS USERS COM DOMINIO net. Quantos bananas tem esse dominio:",
        Banana,
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
  }

  static async getCountDomain(req, res) {
    const { dominio } = req.query;

    if (dominio != "") {
      try {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter((user) =>
          user.website.endsWith(dominio)
        );
        const count = users.length;
        res.status(200).json({
          message: `AQUI ESTÃO OS USERS COM DOMINIO ${dominio}: ${count}`,
          users,
        });
      } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Deu ruim", error });
      }
    }
  }
};
