const connect = require("../db/connect");

module.exports = class dbController{
    static async getTables(req, res){
        const queryShowTables = "show tables";
        connect.query(queryShowTables, async function(err, result, fields){
            if(err){
                console.log(err);
                return res.status(500).json({error: "Erro ao obter tabelas do banco de dados"});
            }
            const tableNames = result.map(row => row[fields[0].name]);
            console.log("Tablelas do banco de dados:", tableNames);
             //res.status(200).json({message: "Tabelas do banco - forma bruta:", result, tables: tableNames}); 

             const tables = [];

             for(let i = 0; i < result.length; i++){
                const tableNames = result[i][`Tables_in_${connect.config.connectionConfig.database}`];

                const queryDescTable = `describe ${tableNames}`;

                try{
                    const tableDescription = await new  Promise((resolve, reject) => {
                        connect.query(queryDescTable, function (err, result, fields) {
                            if (err) {
                                reject(err);
                            }
                            resolve(result);
                        });
                    });
                    tables.push({names: tableNames, description: tableDescription});

                }catch (error){
                    console.log(error);
                    return res.status(500).json({error: "erro ao obiter a descrição da tabela!"});
                }
            }
            res.status(200).json({message: "obtendo todas as tabelas de suas descrições", tables});
        });
    }
}