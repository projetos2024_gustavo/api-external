const router = require('express').Router();
const teacherController = require("../controller/teacherController");
const AlunoController = require("../controller/AlunoController");
const JSONPlaceholderController = require("../controller/JSONPlaceholderController");
const dbController = require("../controller/dbController");


router.get('/ALGUMA-COISA/', teacherController.getAlgumaCoisa);
router.post('/SENAI/', AlunoController.postDocente);
router.put('/listar/', AlunoController.updateDocente);
router.delete('/deletarAluno/:id', AlunoController.deletarAluno);
router.get("/external/io",JSONPlaceholderController.getUsersWebsiteIO);
router.get("/external/net",JSONPlaceholderController.getUsersWebsiteNET);
router.get("/external/com",JSONPlaceholderController.getUsersWebsiteCOM);
router.get("/external/filter",JSONPlaceholderController.getCountDomain);
router.get("/tables", dbController.getTables);


module.exports = router