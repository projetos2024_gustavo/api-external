const express = require('express')
const app = express()
const testConnect = require('./db/testConnect')

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
      this.testConnect = testConnect();
    }

    middlewares() {
      this.express.use(express.json());
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes')
      this.express.use('/Flamengo/', apiRoutes);
    }
  }

  module.exports = new AppController().express;
